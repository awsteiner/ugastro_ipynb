ugastro_ipynb
=============

Jupyter notebooks for Introductory Astrophysics. I used these for
in-class sessions where the students have about 40 minutes to complete
them on laptops or tablets. I typically create a JupyterHub on an
Amazon Web Services instance with a public IP address so that the
students do not have to reconfigure their personal machine.

These notebooks are under construction and not yet fully
self-contained. Your mileage may vary.

Nbviewer links:

https://nbviewer.jupyter.org/urls/gitlab.com/awsteiner/ugastro_ipynb/raw/main/sun_fraunhofer.ipynb
https://nbviewer.jupyter.org/urls/gitlab.com/awsteiner/ugastro_ipynb/raw/main/solar_system_verlet.ipynb
https://nbviewer.jupyter.org/urls/gitlab.com/awsteiner/ugastro_ipynb/raw/main/exoplanets.ipynb
https://nbviewer.jupyter.org/urls/gitlab.com/awsteiner/ugastro_ipynb/raw/main/hipparcos_HR.ipynb
https://nbviewer.jupyter.org/urls/gitlab.com/awsteiner/ugastro_ipynb/raw/main/gais_bb_parallax.ipynb
https://nbviewer.jupyter.org/urls/gitlab.com/awsteiner/ugastro_ipynb/raw/main/tides.ipynb
https://nbviewer.jupyter.org/urls/gitlab.com/awsteiner/ugastro_ipynb/raw/main/ang_mom_solar_wind.ipynb

Licensing
---------

The python code I have created here is licensed
[CC-BY-NC](https://gitlab.com/awsteiner/ugastro_ipynb/blob/main/LICENSE.md).
There are a few functions which are obtained from elsewhere with their
own copyrights and licenses.

Startup
-------

- https://tljh.jupyter.org/en/latest/install/amazon.html

User data
---------

```
#!/bin/bash
echo "Starting user script"
curl https://raw.githubusercontent.com/jupyterhub/the-littlest-jupyterhub/master/bootstrap/bootstrap.py \
| sudo python3 - --admin tljhadmin
echo "Ending user script"
```

Connecting
----------

ssh -i ~/.ssh/tljh_pair.pem ubuntu@ip.address.com

Customization
-------------

apt packages:
- texlive
- texlive-extra
- dvipng

snap package:
- o2scl

install packages (in terminal in tljh, not in ssh terminal)
`sudo -E pip install ...`

- matplotlib
- scipy
- sympy
- astropy
- o2sclpy
- yt
- skyfield

Helpful tricks
--------------

- `sudo journalctl -u jupyter-username`
- `sudo journalctl -f`

Misc
----

- Planetary ephemeris, 
https://naif.jpl.nasa.gov/pub/naif/generic_kernels/spk/planets/de430.bsp

